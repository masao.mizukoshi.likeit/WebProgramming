package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();

		User status= (User) session.getAttribute("userInfo");

		if(status ==null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力


		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		User user =userDao.DetailInfo(id);
		session.setAttribute("userDetail", user);
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		// リクエストパラメータの入力項目を取得
		String id=request.getParameter("id");
		String name= request.getParameter("name");
		String birthDate= request.getParameter("birthDate");
		String password= request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");

		UserDao userDao = new UserDao();
		if(!(password.equals(passwordCheck))) {
			request.setAttribute("errMsg", "パスワードが異なります。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if((name.equals(""))||(birthDate.equals(""))){
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(password.equals("")){
			userDao.UpdateUser1(id,name,birthDate);
			response.sendRedirect("UserListServlet");
			return;
		}


		userDao.UpdateUser2(id,name,birthDate,password);
		session.removeAttribute("userDetail");
		response.sendRedirect("UserListServlet");

	}

}
