<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="date" class="java.util.Date"/>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザ一覧</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body >
  	<nav class="navbar navbar-expand-sm bg-primary navbar-light justify-content-between">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <h3><a class="nav-link text-white" href="#">ユーザ管理システム</a></h3>
    </li>
  </ul>
   <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link text-white" href="LogoutServlet">ログアウト</a>
    </li>
  </ul>
</nav>

    <div class="row justify-content-center">
      <div class="jumbotron text-center col-sm-5">
     <div class="row justify-content-between">

      <div><h5>${userInfo.name}さん</h5></div>
      	<!-- 管理者の主-->
		<c:if test="${userInfo.id == 1}" var="admin"/>
	  <div>
	  <c:if test="${admin}" >
	  <a href="UserAddServlet" class="btn btn-outline-primary">新規登録</a>
	  </c:if>
	  </div>
     </div>
      <c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" style="font-size:12px" role="alert">
		 			 ${errMsg}
				</div>
				</c:if>
        <h4 >ユーザ一覧</h4>
        <br>
        <form action="UserListServlet" method="post">
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-6">
                <input type="text" class="form-control" name="loginId" placeholder="ログインID">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-6">
                <input type="text" class="form-control" name="name" placeholder="ユーザ名">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-3">
              	<input type="date" name="birthDate1" max="2020-12-31" class="form-control">
              </div>
              -
              <div class="col-sm-3">
                 	<input type="date" name="birthDate2" max="2020-12-31" class="form-control">
                 </div>
            </div>
          </div>
          <button type="submit" class="btn btn-primary">検索</button>
        </form>
        <div class="container">
         <br>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>ログインID</th>
        <th>ユーザ名</th>
        <th>生年月日</th>
        <th></th>
      </tr>
    </thead>
	<!--全てのユーザの情報を表示-->
    <c:forEach var="user" items="${userList}">
    <tbody>
      <tr>
        <td>${user.loginId}</td>
        <td>${user.name}</td>
        <td><fmt:formatDate value="${user.birthDate}" type="DATE" dateStyle="FULL" /></td>
        <td>
		<!-- 管理者がログインした場合、以下のボタンを表示 -->
		<c:if test="${admin}" >
        <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
        <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
        <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
        </c:if>
        <!-- 管理者以外ログインした場合、詳細ボタンのみ表示 -->
        <c:if test="${!admin}" >
        <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
        <c:if test="${userInfo.id.equals(user.id)}" >
        <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
        </c:if>
        </c:if>
        </td>
      </tr>
    </tbody>
    </c:forEach>
  </table>
      </div>
      </div>
    </div>
  </body>
</html>