<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>削除</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body >
  	<nav class="navbar navbar-expand-sm bg-primary navbar-light justify-content-between">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <h3><a class="nav-link text-white" href="#">ユーザ管理システム</a></h3>
    </li>
  </ul>
   <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link text-white" href="LogoutServlet">ログアウト</a>
    </li>
  </ul>
</nav>

    <div class="row justify-content-center">
      <div class="jumbotron text-center col-sm-5">
     <div class="row justify-content-between">
      <div><h5>${userInfo.name}さん</h5></div>
    </div>
        <h4 class="text-center">ユーザ削除確認</h4>
        <br>

           <div class="form-group">
            <div class="row justify-content-center">

              <div class="col-sm-10">
                <p  style="font-size: 18px">${userDetail.loginId}を本当に削除してよろしいでしょうか。</p>
              </div>
            </div>
          </div>

          <form action="UserDeleteServlet" method="post">
          <input type="hidden" name="id" value="${userDetail.id}">
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-5">
              <a class="btn btn-primary" href="UserListServlet">キャンセル</a>

              </div>
              <div class="col-sm-5 ">
                <button type="submit" class="btn btn-primary">OK</button>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
  </body>
</html>