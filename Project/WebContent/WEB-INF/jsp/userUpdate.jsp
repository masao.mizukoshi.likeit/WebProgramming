<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>更新</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body >
  	<nav class="navbar navbar-expand-sm bg-primary navbar-light justify-content-between">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <h3><a class="nav-link text-white" href="#">ユーザ管理システム</a></h3>
    </li>
  </ul>
   <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link text-white" href="LogoutServlet">ログアウト</a>
    </li>
  </ul>
</nav>

    <div class="row justify-content-center">
      <div class="jumbotron text-center col-sm-5">
     <div class="row justify-content-between">
      <div><h5>${userInfo.name}さん</h5></div>
    </div>
    <c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" style="font-size:12px" role="alert">
		 			 ${errMsg}
				</div>
				</c:if>
        <h4 class="text-center">ユーザ情報更新</h4>
        <br>
        <form action="UserUpdateServlet" method="POST">
        	<input type="hidden" name="id" value="${userDetail.id}">
           <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-3">
                <p class="text-right" style="font-size: 18px">ログインID</p>
              </div>
              <div class="col-sm-5">
                <p class="text-left" style="font-size: 18px">${userDetail.loginId}</p>
              </div>
            </div>
          </div>
           <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-3">
                <p class="text-right" style="font-size: 18px">パスワード</p>
              </div>
              <div class="col-sm-5">
                <input type="password" class="form-control" name="password">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-3">
                <p class="text-right" style="font-size: 18px">パスワード(確認)</p>
              </div>
              <div class="col-sm-5">
                <input type="password" class="form-control" name="passwordCheck" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-3">
                <p class="text-right" style="font-size: 18px">ユーザ名</p>
              </div>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="name" value="${userDetail.name}">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-3">
                <p class="text-right" style="font-size: 18px">生年月日</p>
              </div>
              <div class="col-sm-5 text-left">
                <input type="date" name="birthDate" max="2020-12-31" class="form-control" value="${userDetail.birthDate}">
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-primary">更新</button>
            <div class="row">
         <a href="UserListServlet" class="col-sm-2">戻る</a>
         </div>
        </form>
      </div>
    </div>
  </body>
</html>