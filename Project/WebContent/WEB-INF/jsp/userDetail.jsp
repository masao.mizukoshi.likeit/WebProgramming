<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="date" class="java.util.Date"/>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>詳細</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body >
  <nav class="navbar navbar-expand-sm bg-primary navbar-light justify-content-between">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <h3><a class="nav-link text-white" href="#">ユーザ管理システム</a></h3>
    </li>
  </ul>
   <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link text-white" href="LogoutServlet">ログアウト</a>
    </li>
  </ul>
</nav>

    <div class="row justify-content-center">
      <div class="jumbotron text-center col-sm-5">
     <div class="row justify-content-between">
      <div><h5>${userInfo.name}さん</h5></div>
    </div>
        <h4 class="text-center">ユーザ情報詳細参照</h4>
        <br>

           <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-4">
                <p class="text-right" style="font-size: 18px">ログインID</p>
              </div>
              <div class="col-sm-5">
                <p class="text-left" style="font-size: 18px">${userDetail.loginId}</p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-4">
                <p class="text-right" style="font-size: 18px">ユーザ名</p>
              </div>
              <div class="col-sm-5">
                <p class="text-left" style="font-size: 18px">${userDetail.name}</p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-4">
                <p class="text-right" style="font-size: 18px">生年月日</p>
              </div>
              <div class="col-sm-5 text-left">
                <p class="text-left" style="font-size: 18px"><fmt:formatDate value="${userDetail.birthDate}" type="DATE" dateStyle="FULL" /></p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-4">
                <p class="text-right" style="font-size: 18px">登録日時</p>
              </div>
              <div class="col-sm-5 text-left">
                <p class="text-left" style="font-size: 18px">${userDetail.createDate}</p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-4">
                <p class="text-right" style="font-size: 18px">更新日時</p>
              </div>
              <div class="col-sm-5 text-left">
                <p class="text-left" style="font-size: 18px">${userDetail.createDate}</p>
              </div>
            </div>
          </div>
            <a class="nav-link text-left" href="UserListServlet">戻る</a>
      </div>
    </div>
  </body>
</html>