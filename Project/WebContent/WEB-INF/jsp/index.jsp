<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ログイン画面</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body >

  	<nav class="navbar navbar-expand-sm bg-primary navbar-light justify-content-between">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <h3><a class="nav-link text-white" href="#">ユーザ管理システム</a></h3>
    </li>
  </ul>
   <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="#"></a>
    </li>
  </ul>
</nav>

    <div class="row justify-content-center">

      <div class="jumbotron text-center col-sm-5">
      <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
        <h4 class="text-center">ログイン画面</h4>
        <br>
        <form action="LoginServlet" method="post">
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-6">
                <input type="text" class="form-control" name="loginId" placeholder="ログインID">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-6">
                <input type="password" class="form-control" name="password" placeholder="パスワード">
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-primary">ログイン</button>
        </form>
      </div>
    </div>
  </body>
</html>