<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>新規登録</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body >
  	<nav class="navbar navbar-expand-sm bg-primary navbar-light justify-content-between">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <h3><a class="nav-link text-white" href="#">ユーザ管理システム</a></h3>
    </li>
  </ul>
   <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link text-white" href="LogoutServlet">ログアウト</a>
    </li>
  </ul>
</nav>

    <div class="row justify-content-center">
      <div class="jumbotron text-center col-sm-5">
     <div class="row justify-content-between">
      <div><h5>${userInfo.name}さん</h5></div>

     </div>
     <c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" style="font-size:12px" role="alert">
		 			 ${errMsg}
				</div>
				</c:if>
        <h4 class="text-center">ユーザ新規登録</h4>
        <br>
        <form action="UserAddServlet" method="post">
           <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-3">
                <p class="text-right" style="font-size: 18px">ログインID</p>
              </div>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="loginId" placeholder="例:0001">
              </div>
            </div>
          </div>
           <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-3">
                <p class="text-right" style="font-size: 18px">パスワード</p>
              </div>
              <div class="col-sm-5">
                <input type="password" class="form-control" name="password" placeholder="●●●●●">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-3">
                <p class="text-right" style="font-size: 18px">パスワード(確認)</p>
              </div>

              <div class="col-sm-5">
                <input type="password" class="form-control" name="passwordCheck" placeholder="●●●●●">
              </div>
            </div>

          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-3">
                <p class="text-right" style="font-size: 18px">ユーザ名</p>
              </div>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="name" placeholder="例:田中太郎">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row justify-content-center">
              <div class="col-sm-3">
                <p class="text-right" style="font-size: 18px">生年月日</p>
              </div>
              <div class="col-sm-5 text-left">
                <input type="date" name="birthDate" max="2020-12-31" class="form-control">
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-primary">登録</button>
        </form>
        <div class="row">
         <a href="UserListServlet" class="col-sm-2">戻る</a>
         </div>
      </div>
    </div>
  </body>
</html>