
CREATE Database UserManagementSample DEFAULT CHARACTER SET utf8;

use UserManagementSample;

CREATE TABLE user(
				id SERIAL PRIMARY KEY AUTO_INCREMENT,
				login_id VARCHAR(255) UNIQUE NOT NULL,
				name VARCHAR(255) NOT NULL,
				birth_date DATE NOT NULL,
				password VARCHAR(255) NOT NULL,
				create_date DATETIME NOT NULL,
				update_date DATETIME NOT NULL
				);
--あらかじめ管理者として以下のデータを登録しておく--
INSERT into user(
				login_id,name,
				birth_date,
				password,
				create_date,
				update_date) 
				VALUE(
					"admin",
					"管理者",
					"1994-09-06",
					MD5("password"),
					"2019-07-05 11:29:36",
					"2019-07-05 11:29:36"
					);
--パスワードをMD5に暗号化--
mysql> update user set password =MD5("password") where id =1;
